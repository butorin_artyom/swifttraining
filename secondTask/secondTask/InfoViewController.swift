//
//  InfoViewController.swift
//  secondTask
//
//  Created by Артём Буторин on 15.09.21.
//

import UIKit

class InfoViewController: UIViewController {
    
    private(set) lazy var logoImage: UIImageView = {
        let imageView = UIImageView(image: UIImage())
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        imageView.layer.cornerRadius = 50
        imageView.backgroundColor = .systemGray5
        imageView.clipsToBounds = false
        return imageView
    }()
    
    private(set) lazy var titleInfoVC: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 26)
        return label
    }()
    
    
    private(set) lazy var descriptionInfoVC: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 26)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.constraints()
    }
    
    private func initView() {
        self.view.addSubview(self.logoImage)
        self.view.addSubview(self.titleInfoVC)
        self.view.addSubview(self.descriptionInfoVC)
        self.view.backgroundColor = .systemBackground
    }
    
    private func constraints() {
        self.logoImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.logoImage.bottomAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        self.logoImage.widthAnchor.constraint(equalToConstant:100).isActive = true
        self.logoImage.heightAnchor.constraint(equalToConstant:100).isActive = true
        
        self.titleInfoVC.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.titleInfoVC.bottomAnchor.constraint(equalTo: self.logoImage.bottomAnchor, constant: +50).isActive = true
        
        self.descriptionInfoVC.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.descriptionInfoVC.bottomAnchor.constraint(equalTo: self.titleInfoVC.bottomAnchor, constant: +50).isActive = true
    }
}
