//
//  ViewController.swift
//  secondTask
//
//  Created by Артём Буторин on 13.09.21.
//

import UIKit

class ViewController: UIViewController {
    
    
    let tableView = UITableView()
    private(set) lazy var data = [Data]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(tableView)
        self.createDataArray()
        
        self.tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = view.bounds
    }
    
    private func createDataArray() {
        for x in 1...1000{
            data.append(Data(image: UIImage(named: "MainImage")!,title: "Title \(x)" ,description: "Description \(x)"))
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomTableViewCell
        cell.data = data[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let rootVC = InfoViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        rootVC.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(diismiss))
        
        rootVC.descriptionInfoVC.text = "Description \(indexPath.row + 1)"
        rootVC.titleInfoVC.text = "Title \(indexPath.row + 1)"
        rootVC.logoImage.image = UIImage(named: "MainImage")
        
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
    }
    
    @objc private func diismiss() {
        dismiss(animated: true, completion: nil)
    }
    
}


