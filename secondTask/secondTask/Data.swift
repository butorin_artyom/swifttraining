//
//  Data.swift
//  secondTask
//
//  Created by Артём Буторин on 16.09.21.
//

import UIKit
struct Data {
    var image: UIImage
    var title: String
    var description: String
}
