import UIKit
class CustomTableViewCell : UITableViewCell {
        
    var data : Data? {
        didSet {
            dataTitleLabel.text = data?.title
            dataDescriptionLabel.text = data?.description
            dataImage.image = data?.image
        }
    }
    
    private let dataTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dataDescriptionLabel: UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    private var dataImage: UIImageView = {
        let imageView = UIImageView(image: UIImage())
        imageView.contentMode = .scaleAspectFit
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        imageView.backgroundColor = .systemGray5
        imageView.layer.cornerRadius = 25
        imageView.clipsToBounds = true
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initView()
        self.constraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initView() {
        self.addSubview(self.dataImage)
        self.addSubview(self.dataTitleLabel)
        self.addSubview(self.dataDescriptionLabel)
    }
    
    private func constraints() {
        self.dataImage.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil,
                              paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0,
                              width: 50, height: 50, enableInsets: false)
        self.dataTitleLabel.anchor(top: topAnchor, left: dataImage.rightAnchor, bottom: nil, right: nil,
                                   paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,
                                   width: frame.size.width / 2, height: 0, enableInsets: false)
        self.dataDescriptionLabel.anchor(top: dataTitleLabel.bottomAnchor, left: dataImage.rightAnchor, bottom: nil, right: nil,
                                         paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 0,
                                         width: frame.size.width / 2, height: 0, enableInsets: false)
    }
}
