//
//  TableVC.swift
//  thirdTask
//
//  Created by Артём Буторин on 25.09.21.
//

import Contacts
import ContactsUI
import UIKit
import CoreData

class TableVC: UIViewController, UITableViewDataSource, CNContactPickerDelegate, UITableViewDelegate {
    
    let mainVC = ViewController()
    
    public let table: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        return table
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    lazy var phoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Выберите номер"
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    lazy var emailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(table)
        self.view.addSubview(nameLabel)
        self.view.addSubview(phoneLabel)
        self.view.addSubview(emailLabel)
        self.view.backgroundColor = .systemBackground
        self.table.frame          = view.bounds
        self.table.dataSource     = self
        self.table.delegate       = self
        
        let fetchRequest: NSFetchRequest<ContactDataBase> = ContactDataBase.fetchRequest()
        do {
            let users = try PersistenceCervise.context.fetch(fetchRequest)
            self.mainVC.users = users
            table.reloadData()
        } catch {
            print("Error")
        }
        
        self.constraints()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainVC.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = mainVC.users[indexPath.row].phoneNumber
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        UserDefaults.standard.set(mainVC.users[indexPath.row].firstName, forKey: "firstName")
        UserDefaults.standard.set(mainVC.users[indexPath.row].lastName, forKey: "lastName")
        UserDefaults.standard.set(mainVC.users[indexPath.row].email, forKey: "email")
        UserDefaults.standard.set(mainVC.users[indexPath.row].phoneNumber, forKey: "phoneNumber")
        
        guard let firstName = UserDefaults.standard.string(forKey: "firstName") else { return }
        guard let lastName  = UserDefaults.standard.string(forKey: "lastName") else { return }
        let fullName = firstName + " " + lastName
        nameLabel.text  = fullName
        phoneLabel.text = UserDefaults.standard.string(forKey: "phoneNumber")
        emailLabel.text = UserDefaults.standard.string(forKey: "email")
    }
    
    private func constraints() {
        self.table.leftAnchor.constraint(equalTo: view.leftAnchor).isActive                                    = true
        self.table.topAnchor.constraint(equalTo: view.topAnchor).isActive                                      = true
        self.table.rightAnchor.constraint(equalTo: view.rightAnchor).isActive                                  = true
        self.table.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -150.0).isActive              = true
        
        self.nameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive                  = true
        self.nameLabel.centerYAnchor.constraint(equalTo: self.table.bottomAnchor, constant: 20).isActive       = true
        
        self.phoneLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive                 = true
        self.phoneLabel.centerYAnchor.constraint(equalTo: self.nameLabel.bottomAnchor, constant: 20).isActive  = true
        
        self.emailLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive                 = true
        self.emailLabel.centerYAnchor.constraint(equalTo: self.phoneLabel.bottomAnchor, constant: 20).isActive = true
    }
    
}
