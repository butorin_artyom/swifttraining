//
//  ViewController.swift
//  thirdTask
//
//  Created by Артём Буторин on 25.09.21.
//

import UIKit
import Contacts
import ContactsUI

class ViewController: UIViewController, CNContactPickerDelegate {
    
    var contactAccess: Bool? = nil
    var users                = [ContactDataBase]()
    
    private(set) lazy var selectContact: CustomButton = {
        let button = CustomButton(type: .system)
        button.setTitle("Выбрать контакт", for: .normal)
        button.addTarget(self, action: #selector(self.selectContactTap), for: .touchUpInside)
        return button
    }()
    
    private(set) lazy var showContacts: CustomButton = {
        let button = CustomButton(type: .system)
        button.setTitle("Показать контакты", for: .normal)
        button.addTarget(self, action: #selector(self.showContactsTap), for: .touchUpInside)
        return button
    }()
    
    private(set) lazy var showNumber: CustomButton = {
        let button = CustomButton(type: .system)
        button.setTitle("Показать номер", for: .normal)
        button.addTarget(self, action: #selector(self.showNumberAlert), for: .touchUpInside)
        return button
    }()
    
    @objc func selectContactTap() {
        self.accessTest()
        let vc = CNContactPickerViewController()
        vc.delegate = self
        if contactAccess == true {
            self.present(vc, animated: true)
        } else  if contactAccess == false{
            self.errorAlert()
        }
    }
    
    @objc func showContactsTap() {
        let rootVC  = TableVC()
        let navVC   = UINavigationController(rootViewController: rootVC)
        rootVC.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(dismissButton))
        navVC.modalPresentationStyle = .fullScreen
        present(navVC, animated: true)
    }
    
    @objc private func dismissButton() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func showNumberAlert() {
        let phoneNumberDefaults    = UserDefaults.standard.string(forKey: "phoneNumber")
        var phoneNumberTest        = String()
        
        if phoneNumberDefaults == nil {
            phoneNumberTest = "Номер не сохранен"
        } else {
            phoneNumberTest = phoneNumberDefaults!
        }
        
        let alertController = UIAlertController(title: "Сохраненный номер:",
                                                message: phoneNumberTest,
                                                preferredStyle: .actionSheet)
        let action          = UIAlertAction(title: "OK",
                                            style: .default,
                                            handler: .none)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        var email       = String()
        let numbers     = contact.phoneNumbers.first
        let firstName   = contact.givenName
        let lastName    = contact.familyName
        let phoneNumber = (numbers?.value)?.stringValue ?? ""
        
        let userData    = ContactDataBase(context: PersistenceCervise.context)
        
        if let emailValue: CNLabeledValue = contact.emailAddresses.first {
            email = emailValue.value as String
        } else {
            email = "email не указан"
        }
        
        self.successfulAlert()
        
        userData.firstName   = firstName
        userData.lastName    = lastName
        userData.phoneNumber = phoneNumber
        userData.email       = email
        
        PersistenceCervise.saveContext()
        self.users.append(userData)
    }
    
    func successfulAlert() {
        let alertController = UIAlertController(title: "Сохранено",
                                                message: "Успешно",
                                                preferredStyle: .actionSheet)
        self.presentedViewController?.present(alertController, animated: true, completion: nil)
    }
    
    
    func errorAlert() {
        let alertController = UIAlertController(title: "Ошибка доступа",
                                                message: "Нет доступа к контактам, пожалуйста, дайте доступ в настройках устройства в разделе «Конфиденциальность»",
                                                preferredStyle: .alert)
        let action          = UIAlertAction(title: "OK",
                                            style: .default,
                                            handler: .none)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func accessTest() {
        CNContactStore().requestAccess(for: .contacts) { (access, error) in
            self.contactAccess = access
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
        self.constraints()
    }
    
    private func initView() {
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(self.selectContact)
        self.view.addSubview(self.showContacts)
        self.view.addSubview(self.showNumber)
    }
    
    private func constraints() {
        
        self.selectContact.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.selectContact.bottomAnchor.constraint(equalTo: self.showContacts.topAnchor,
                                                   constant: -20).isActive                = true
        
        self.showContacts.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive  = true
        self.showContacts.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive  = true
        
        self.showNumber.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive    = true
        self.showNumber.topAnchor.constraint(equalTo: self.showContacts.bottomAnchor,
                                             constant: 20).isActive                       = true
    }
}

