//
//  ContactDataBase+CoreDataProperties.swift
//  thirdTask
//
//  Created by Артём Буторин on 25.09.21.
//
//

import Foundation
import CoreData


extension ContactDataBase {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ContactDataBase> {
        return NSFetchRequest<ContactDataBase>(entityName: "ContactDataBase")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var email: String?

}

extension ContactDataBase : Identifiable {

}
